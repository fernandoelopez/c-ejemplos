#include <stdio.h>

enum {
    INPUT,
    OUTPUT,
    DEBUG,
};

int procesarargs(int cant, char **args){
    /*
     * -i
     * -o
     * -d
     */
    for (int i = 1; i < cant; i++){
        if (args[i][0] != '-') continue;
        switch(args[i][1]){
            case 'i':
                return INPUT;
            case 'o':
                return OUTPUT;
            case 'd':
                return DEBUG;
        }
    }
    return -1;
}

int main(int argc, char **argv){
    printf(
        "Seleccionó %d\n",
        procesarargs(argc, argv)
    );

}
