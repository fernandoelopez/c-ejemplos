#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    int num = 5;
    int *ptr = malloc(sizeof(int));
    *ptr = num;

    int arr[] = {1, 2, 3, 4, 5};
    int *ptr2 = malloc(sizeof(arr));

    /*
    for (int i = 0; i < 5; i++){
        ptr2[i] = arr[i];
    }
    */
    memcpy(ptr2, arr, sizeof(arr));
    free(ptr);
    free(ptr2);
}
