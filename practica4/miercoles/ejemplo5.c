#include <string.h>
#include <stdlib.h>
struct persona{
    int dni;
    char *nombre;
    char dir[200];
};

void cambiar_nombre(struct persona *p, char *n){
    strcpy(p->nombre, n);
}

int main(){
    struct persona yo;
    struct persona *ptr;

    ptr = malloc(sizeof(struct persona));

    yo.dni = 313131;
    strcpy(yo.nombre, "Fernando");
    cambiar_nombre(&yo, "Esteban");
}
