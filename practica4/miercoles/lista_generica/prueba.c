#include "lista.h"
#include <string.h>
#include <stdio.h>

char *cadenas[] = {
    "hola",
    "chau",
    "hola",
    "hola",
    "hola",
};

int compara(char *a, char *b){
    return 1;
}

int main(){
    char buscar[] = "hola";
    int encontrado;
    lista_t li;
    lista_ini(&li);
    for (int i = 0; i < 2; i++){
        lista_agregar(&li, cadenas[i]);
    }
    encontrado = lista_existe(li, buscar, (fcomparacion) strcmp);
    printf("%d\n", encontrado);
    encontrado = lista_existe(li, "dia", (fcomparacion) strcmp);
    printf("%d\n", encontrado);
}
