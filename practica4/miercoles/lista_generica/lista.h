#ifndef LISTA_H
#define LISTA_H

typedef struct nodo nodo_t;
struct nodo {
    void *dato;
    nodo_t *sig;
};

typedef nodo_t *lista_t;
typedef int (*fcomparacion)(void *, void *);

void lista_ini(lista_t *li);
void lista_agregar(lista_t *li, void *dato);
int lista_existe(
    lista_t li,
    void *dato,
    int (*cmp)(void *, void *)
);

#endif
