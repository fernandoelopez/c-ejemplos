#include "lista.h"
#include <stdlib.h>

void lista_ini(lista_t *li){
    *li = NULL;
}

void lista_agregar(lista_t *li, void *dato){
    nodo_t *nuevo = malloc(sizeof(nodo_t));
    nuevo->dato = dato;
    nuevo->sig = *li;
    *li = nuevo;
}

int lista_existe(
    lista_t li,
    void *dato,
    int (*cmp)(void *, void *)
){
    nodo_t *actual = li;
    while (actual != NULL && cmp(actual->dato, dato)){
        actual = actual->sig;
    }
    if (actual == NULL) return 0;
    return 1;
}
