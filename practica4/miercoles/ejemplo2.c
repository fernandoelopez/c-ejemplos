#include <stdio.h>
#include <string.h>
enum {
    INPUT,
    OUTPUT,
    DEBUG,
};

int procesarargs(int cant, char **args){
    /*
     * -i
     * -o
     * -d
     */
    for (int i = 1; i < cant; i++){
        if (strcmp("-i", args[i]) == 0){
            return INPUT;
        }
        else if (strcmp("-o", args[i]) == 0){
            return OUTPUT;
        }
        else if (strcmp("-d", args[i]) == 0){
            return DEBUG;
        }
    }
    return -1;
}

int main(int argc, char **argv){
    printf(
        "Seleccionó %d\n",
        procesarargs(argc, argv)
    );

}
