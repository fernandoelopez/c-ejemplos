#include "pila.h"
#include <stdio.h>

pila_int_t pila_int_create(){
    pila_int_t p;
    p.tam = 0;
    return p;
}

int pila_int_push(pila_int_t *pila, int dato){
    if (pila->tam < 1024){
        pila->dato[pila->tam] = dato;
        pila->tam++;
        return 1;
    }
    return 0;
}

int pila_int_top(pila_int_t pila){
    return pila.dato[pila.tam - 1];
}

int pila_int_pop(pila_int_t *pila){
    int dato = pila->dato[pila->tam - 1];
    pila->tam--;
    return dato;
}

void pila_int_print(pila_int_t pila){
    for (int i = pila.tam - 1; i >= 0; i--){
        printf("%d, ", pila.dato[i]);
    }
    puts("");
}

int pila_int_len(pila_int_t pila){
    return pila.tam;
}
