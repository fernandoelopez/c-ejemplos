#ifndef PILA_H
#define PILA_H

typedef struct pila_int {
    int dato[1024];
    int tam;
} pila_int_t;

pila_int_t pila_int_create();
int pila_int_push(pila_int_t *, int);
int pila_int_top(pila_int_t);
int pila_int_pop(pila_int_t *);
void pila_int_print(pila_int_t);
int pila_int_len(pila_int_t);
#endif
