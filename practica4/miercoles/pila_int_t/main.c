#include <stdio.h>
#include "pila.h"

// Compilar con:
//  - gcc -Wall -o programa main.c pila.c

int main(){
    int nro;
    // Inicialización
    pila_int_t pila = pila_int_create();
    for (int i = 0; i < 5; i++){
        // Push
        if (!pila_int_push(&pila, i)){
            printf("Error insertando\n");
        }
    }
    // Print
    pila_int_print(pila);

    // Top
    printf("Tope %d\n", pila_int_top(pila));

    // Pop
    nro = pila_int_pop(&pila);
    printf("Elemento sacado con pop: %d\n", nro);

    pila_int_print(pila);

    // Len
    printf("Longitud %d\n", pila_int_len(pila));

    return 0;
}
