#include <string.h>
#include <stdio.h>
struct persona {
    char nombre[40];
    unsigned edad;
    char direccion[100];
};
void cambiar_edad(struct persona *p){
    //(*p).edad = 40;
    p->edad = 40;
}

int main(){
    struct persona yo;
    yo.edad = 32;
    strncpy(yo.nombre, "Fernando", 39);
    yo.nombre[39] = '\0';
    puts(yo.nombre);
    cambiar_edad(&yo);
}
