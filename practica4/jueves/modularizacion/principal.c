#include "lista.h"
#include <stdlib.h>

int main(){
    int error;
    lista_t lista;
    lista = crear_lista();

    for (int i = 0; i < 20; i++){
        error = insertar_lista(&lista, i);
        if (error) exit(1);
    }
}
