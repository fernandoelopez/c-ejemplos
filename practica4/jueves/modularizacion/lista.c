#include "lista.h"
#include <stdlib.h>
lista_t crear_lista(){
    return NULL;
}

int insertar_lista(lista_t *l, int dato){
    struct nodo *nuevo;
    nuevo = malloc(sizeof(struct nodo));
    if (nuevo == NULL) return 0;
    nuevo->dato = dato;
    nuevo->sig = *l;
    *l = nuevo;
    return 1;
}
