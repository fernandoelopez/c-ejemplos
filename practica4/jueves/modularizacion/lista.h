struct nodo {
    int dato;
    struct nodo *sig;
};

typedef struct nodo *lista_t;

lista_t crear_lista();
int insertar_lista(lista_t *l, int dato);
