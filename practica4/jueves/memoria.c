#include <stdlib.h>
#include <string.h>

int main(){
    int arr[] = {1, 2, 3};
    int *ptr = malloc(sizeof(arr));

    /*
    for (int i = 0; i < 3; i++){
        ptr[i] = arr[i];
    }
    */
    memcpy(ptr, arr, sizeof(arr));

    free(ptr);
}
