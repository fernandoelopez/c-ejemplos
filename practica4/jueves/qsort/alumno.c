#include "alumno.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void talumno_ini(
    T_alumno *alumno,
    char *nombre,
    char *apellido,
    char *fecha,
    int edad
){
    alumno->nombre = malloc(strlen(nombre) + 1);
    strcpy(alumno->nombre, nombre);
    alumno->apellido = malloc(strlen(apellido) + 1);
    strcpy(alumno->apellido, apellido);
    strcpy(alumno->fecha, fecha);
    alumno->edad = edad;
}

T_alumno talumno_ini2(
    char *nombre,
    char *apellido,
    char *fecha,
    int edad
){
    T_alumno a;
    a.nombre = malloc(strlen(nombre) + 1);
    strcpy(a.nombre, nombre);
    a.apellido = malloc(strlen(apellido) + 1);
    strcpy(a.apellido, apellido);
    strcpy(a.fecha, fecha);
    a.edad = edad;
    return a;
}

void talumno_eliminar(T_alumno *a){
    free(a->nombre);
    free(a->apellido);
    a->nombre = NULL;
    a->apellido = NULL;
}

void talumno_imprimir(T_alumno a){
    printf("%s %s %s %d\n",
            a.nombre,
            a.apellido,
            a.fecha,
            a.edad);
}

int talumno_cmp_edad(
    const void *a,
    const void *b){
    int edad_a = ((T_alumno *) a)->edad;
    int edad_b = ((T_alumno *) b)->edad;
    /*
    if (edad_a == edad_b) return 0;
    else if (edad_a < edad_b) return -1;
    else return 1;*/
    return edad_a - edad_b;

}
