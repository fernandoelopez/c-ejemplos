#include "alumno.h"
#include <stdlib.h>
int main(){
    T_alumno alumnos[3];
    talumno_ini(&alumnos[0],
        "Pepe",
        "Gomez",
        "1/2/1990",
        40
    );
    talumno_ini(&alumnos[1],
        "Maria",
        "Rodriguez",
        "1/2/1980",
        41
    );
    talumno_ini(&alumnos[2],
        "Andres",
        "Lopez",
        "1/2/1970",
        20
    );

    qsort(alumnos, 3, sizeof(T_alumno), talumno_cmp_edad);

    for (int i = 0; i < 3; i++){
        talumno_imprimir(alumnos[i]);
    }
    // Liberar
    for (int i = 0; i < 3; i++){
        talumno_eliminar(&alumnos[i]);
    }
}
