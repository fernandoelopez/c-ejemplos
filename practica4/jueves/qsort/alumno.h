#ifndef ALUMNO_H
#define ALUMNO_H

struct alumno{
    char *nombre;
    char *apellido;
    char fecha[512];
    int  edad;
};
typedef struct alumno T_alumno;

void talumno_ini(
    T_alumno *a,
    char *nombre,
    char *apellido,
    char *fecha,
    int edad
);

T_alumno talumno_ini2(
    char *nombre,
    char *apellido,
    char *fecha,
    int edad
);
void talumno_eliminar(T_alumno *a);
void talumno_imprimir(T_alumno a);
int talumno_cmp_edad(
    const void *a,
    const void *b);
#endif
