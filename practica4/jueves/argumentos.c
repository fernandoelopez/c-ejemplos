#include <stdio.h>
#include <string.h>

enum {
    ENTRADA,
    SALIDA,
    DEPURACION,
};

/*
 * -i -> entrada
 * -o -> salida
 * -d -> depuracion
 */
int procesarargs(int argc, char **argv){
    printf("argv[0] = %s\n", argv[0]);
    if (argc == 2){
        if (strcmp(argv[1], "-i") == 0){
            return ENTRADA;
        }
        else if (strcmp(argv[1], "-o") == 0){
            return SALIDA;
        }
        else if (strcmp(argv[1], "-d") == 0){
            return DEPURACION;
        }
        else return -1;
    }
    else return -1;
}

int main(int argc, char **argv){
    int codigo = procesarargs(argc, argv);
    printf("Codigo %d\n", codigo);
}
