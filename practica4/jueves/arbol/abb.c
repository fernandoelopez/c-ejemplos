#include "abb.h"
#include <stdlib.h>
#include <stdio.h>

arbol_t arbol_crear(){
    /**
     * Crea un árbol vacío, como arbol_t es un puntero podemos
     * definir un árbol vacío como un puntero inicializado en NULL.
     */
    return NULL;
}

void arbol_insertar(arbol_t *a, persona_t p){
    /**
     * Inserta un nuevo nodo de forma recursiva manteniendo el
     * orden del árbol binario de búsqueda.
     */
    if (*a == NULL){
        *a = malloc(sizeof(nodo_t));
        (*a)->persona = p;                      //(*(*a)).persona = p;
        (*a)->izq = (*a)->der = arbol_crear();  // Pongo los nodos izq y derecho en NULL
    }
    else{
        if (p.edad < (*a)->persona.edad){
            arbol_insertar(&(*a)->izq, p);      // `arbol_insertar() puede modificar izq si éste es una hoja, por eso
                                                // es necesario enviar la dirección de memoria de izq
                                                // que se accede como siempre con `&`.
        }
        else {
            arbol_insertar(&(*a)->der, p);      // Ídem para der
        }
    }
}


void arbol_insertar_configurable(arbol_t *a, persona_t p, int (*cmp)(persona_t, persona_t)){
    /**
     * Inserta un nuevo nodo de forma recursiva manteniendo el
     * orden del árbol binario de búsqueda. Esta versión recibe un
     * puntero a una función que permite cambiar el orden de inserción.
     */
    if (*a == NULL){
        *a = malloc(sizeof(nodo_t));
        (*a)->persona = p;
        //(*(*a)).persona = p;
        (*a)->izq = (*a)->der = arbol_crear();
    }
    else{
        if (cmp(p, (*a)->persona) < 0){
            arbol_insertar_configurable(&(*a)->izq, p, cmp);
        }
        else {
            arbol_insertar_configurable(&(*a)->der, p, cmp);
        }
    }
}

void arbol_imprimir_enorden(arbol_t a){
    /**
     * Imprime todos los nodos del árbol en orden
     */
    if (a == NULL) return;                                      // Si es una hoja retornar sin hacer nada

    arbol_imprimir_enorden(a->izq);                            // Imprimir el subarbol izquierdo
    printf("%s, %d\n", a->persona.nombre, a->persona.edad);    // Imprimir el nodo actual
    arbol_imprimir_enorden(a->der);                            // Imprimir el subarbol derecho
}

void arbol_liberar(arbol_t a){
    if (a == NULL) return;                                      // Si es una hoja retornar sin hacer nada
    arbol_liberar(a->izq);                                      // Liberar el subarbol izquierdo
    arbol_liberar(a->der);                                      // Liberar el subarbol derecho
    free(a);                                                    // Liberar el nodo actual
}
