#include <stdio.h>
#include <string.h>
#include "abb.h"
#include "persona.h"

int comparar_por_nombre(persona_t p1, persona_t p2);

int main(){
    arbol_t arbol_por_edad;
    arbol_t arbol_por_nombre;

    // Array de 5 personas para inicializar los árboles
    persona_t personas[] = {
        {"Maria", 20},
        {"Juan", 32},
        {"Ana", 15},
        {"Pepe", 10},
        {"Andrea", 35},
    };


    arbol_por_edad = arbol_crear();
    for (int i = 0; i < 5; i++){
        // `arbol_insertar()` puede modificar `arbol_por_edad` por eso está
        // definida para recibir un puntero a la variable.
        arbol_insertar(&arbol_por_edad, personas[i]);
    }
    printf("--- Arbol creado con arbol_insertar() ---\n");
    arbol_imprimir_enorden(arbol_por_edad);
    printf("-----------------------------------------\n");
    arbol_liberar(arbol_por_edad);


    arbol_por_nombre = arbol_crear();
    for (int i = 0; i < 5; i++){
        // `arbol_insertar()` puede modificar `arbol_por_edad` por eso está
        // definida para recibir un puntero a la variable.
        arbol_insertar_configurable(&arbol_por_nombre, personas[i], comparar_por_nombre);
    }
    printf("--- Arbol creado con arbol_insertar_configurable() ---\n");
    arbol_imprimir_enorden(arbol_por_nombre);
    printf("------------------------------------------------------\n");
    arbol_liberar(arbol_por_nombre);

    return 0;
}


int comparar_por_nombre(persona_t p1, persona_t p2){
    // Compara 2 personas por nombre retorna negativo si la de la izquierda
    // tiene un nombre que va antes en el orden lexicográfico, 0 si son iguales
    // y un valor positivo si el de la derecha va antes.
    // La definimos de esta manera copiando lo que hace `strcmp()` y copiando
    // lo que esperan funciones como `qsort()` por conveniencia.

    return strcmp(p1.nombre, p2.nombre);
}
