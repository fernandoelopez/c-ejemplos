#ifndef ABB_H
#define ABB_H
#include "persona.h"
typedef struct nodo nodo_t;
struct nodo {
    persona_t persona;
    nodo_t *izq;
    nodo_t *der;
};
typedef nodo_t *arbol_t;

arbol_t arbol_crear();
void arbol_insertar(arbol_t * a, persona_t p);
void arbol_insertar_configurable(arbol_t *a, persona_t p, int (*cmp)(persona_t, persona_t));
void arbol_imprimir_enorden(arbol_t a);
void arbol_liberar(arbol_t a);

#endif
