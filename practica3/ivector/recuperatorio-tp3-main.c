#include <stdio.h>
#include <stdlib.h>

/*
 * COMPLETAR
 */

int main(){
    int i;
    int a = 0, b = 1;
    int tmp;
    ivector fibo;
    fibo = ivector_crear();

    for (i = 0; i < 10; i++){
        ivector_agregar(&fibo, a);
        tmp = b;
        b = a + b;
        a = tmp;
    }

    for (i = 0; i < 10; i++){
        printf("fib(%d) = %d\n", i, ivector_acceder(fibo, i));
    }

    printf("---- Si elimino la posición 4: ----\n");
    ivector_eliminar(&fibo, 4);
    for (i = 0; i < 9; i++){
        printf("fib(%d) = %d\n", i, ivector_acceder(fibo, i));
    }

}
