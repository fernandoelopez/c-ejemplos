#include <stdio.h>
#include <stdlib.h>

typedef struct ivector {
    int *data;
    unsigned logical_size;
} ivector;

ivector ivector_crear() {
    ivector v = {
        NULL,
        0,
    };
    return v;
}

unsigned ivector_agregar(ivector *v, int elemento) {
    unsigned indice = v->logical_size;

    v->logical_size++;
    v->data = realloc(v->data, v->logical_size * sizeof(int));
    v->data[indice] = elemento;
    return indice;
}

void ivector_eliminar(ivector *v, unsigned indice) {
    int ultimo_indice = v->logical_size - 1;

    for (int i = indice; i < ultimo_indice; i++){
        v->data[i] = v->data[i + 1];
    }

    v->logical_size--;
    if (v->logical_size > 0) {
        v->data = realloc(v->data, v->logical_size * sizeof(int));
    }
    else {
        v->data = NULL;
    }
}

int ivector_acceder(ivector v, unsigned indice) {
    if (indice >= v.logical_size) return -1;

    return v.data[indice];
}

int main(){
    int i;
    int a = 0, b = 1;
    int tmp;
    ivector fibo;
    fibo = ivector_crear();

    for (i = 0; i < 10; i++){
        ivector_agregar(&fibo, a);
        tmp = b;
        b = a + b;
        a = tmp;
    }

    for (i = 0; i < 10; i++){
        printf("fib(%d) = %d\n", i, ivector_acceder(fibo, i));
    }

    printf("---- Si elimino la posición 4: ----\n");
    ivector_eliminar(&fibo, 4);
    for (i = 0; i < 9; i++){
        printf("fib(%d) = %d\n", i, ivector_acceder(fibo, i));
    }

}
