Implementar el tipo de datos ivector (vector de enteros) con las operaciones:
- `ivector ivector_crear()`: crea o inicializa un ivector vacío
- `unsigned ivector_agregar(ivector *v, int elemento)`: agrega un elemento al
  final del ivector (incrementando su tamaño alocado). Retorna el índice en
  el que se encuentra el elemento insertado.
- `void ivector_eliminar(ivector *v, unsigned indice)`: elimina el elementoi
  ubicado en la posición indicada por `indice` (haciendo un corrimiento de los
  datos y decrementando el tamaño alocado).
- `int ivector_acceder(ivector v, unsigned indice)`: retorna el dato almacenado en
  la posición indicada por `indice` o `-1` si el indice está fuera de rango
  (es decir si es mayor que el tamaño lógico del vector).

Vector es el nombre usado en C++ para hacer referencia a un arreglo que puede
crecer o achicarse de forma dinámica al insertar o eliminar sus datos. En C
no contamos con ese tipo de datos en la librería estándar por lo que este
trabajo les pide implementar su propia versión valiéndose de `malloc`, `free`
y `realloc`.

Agregar la implementación en la siguiente plantilla:

```
#include <stdio.h>
#include <stdlib.h>

/*
 * COMPLETAR
 */

int main(){
    int i;
    int a = 0, b = 1;
    int tmp;
    ivector fibo;
    fibo = ivector_crear();

    for (i = 0; i < 10; i++){
        ivector_agregar(&fibo, a);
        tmp = b;
        b = a + b;
        a = tmp;
    }

    for (i = 0; i < 10; i++){
        printf("fib(%d) = %d\n", i, ivector_acceder(fibo, i));
    }

    printf("---- Si elimino la posición 4: ----\n");
    ivector_eliminar(&fibo, 4);
    for (i = 0; i < 9; i++){
        printf("fib(%d) = %d\n", i, ivector_acceder(fibo, i));
    }

}
```

Al completar la implementación en esta plantilla se debería obtener el
siguiente resultado:

```
$ ./recuperatorio-tp3 
fib(0) = 0
fib(1) = 1
fib(2) = 1
fib(3) = 2
fib(4) = 3
fib(5) = 5
fib(6) = 8
fib(7) = 13
fib(8) = 21
fib(9) = 34
---- Si elimino la posición 4: ----
fib(0) = 0
fib(1) = 1
fib(2) = 1
fib(3) = 2
fib(4) = 5
fib(5) = 8
fib(6) = 13
fib(7) = 21
fib(8) = 34
```

Este trabajo se evaluará en la próxima clase de consulta del turno en que
rindieron el parcialito originalmente (semana del 19/6).
