#include <stdio.h>
#include <stdint.h> // int32_t, etc...
#include <inttypes.h> // PRId32, etc...

int main(){
    int32_t a = -4;

    printf("%" PRId32 "\n", a);
    return 0;
}
