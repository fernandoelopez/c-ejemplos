#include <netinet/in.h>
#include <stdio.h>
int main(){
    FILE *f = fopen("hola", "wb");
    for (uint16_t i = 0; i < 5; i++)
    {
        uint16_t bigendian = htons(i);
        fwrite(&bigendian, sizeof(uint16_t), 1, f);
    }
    fclose(f);

    f = fopen("hola", "rb");
    uint16_t datos[5];
    fread(datos, sizeof(uint16_t), 5, f);
    for (uint16_t i = 0; i < 5; i++){
        printf("Original: %d, Convertido: %d\n", datos[i], ntohs(datos[i]));
    }

    return 0;
}
