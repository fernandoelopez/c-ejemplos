#include <stdio.h>

struct fecha {
    unsigned year,
    unsigned month,
    unsigned day,
};
// YYYY/MM/DD
int main(){
    struct fecha fecha;
    int conv;
    FILE *f = fopen("test", "r");
    conv = fscanf(f, "%4u/%2u/%2u\n",
                  &fecha.year,
                  &fecha.month,
                  &fecha.day);
    if (conv != 3) {
        fclose(f);
        return 1;
    }
    fclose(f);
    return 0;

}
