#include <stdio.h>

struct persona {
    char nombre[100];
    char apellido[100];
    unsigned char edad;
};

int main(){
    FILE *f;
    struct persona p;
    strcpy(p.nombre, "Pepe");
    strcpy(p.apellido, "Perez");
    p.edad = 20;
    f = fopen("../hola.txt", "wb");
    fwrite(p.nombre, sizeof(p.nombre), 1, f);
    fwrite(p.apellido, sizeof(p.apellido), 1, f);
    fwrite(&p.edad, sizeof(p.edad), 1, f);

    fclose(f);
}
