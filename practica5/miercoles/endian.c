#include <stdio.h>
#include <netinet/in.h>
#include <stdint.h>
#include <inttypes.h>
int main(){
    // Quiero escribir y leer registros
    // sin signo de 32bits en un archivo
    // que deben codificarse en orden de
    // bytes para red (big endian)
    uint32_t valor;
    uint32_t convertido;
    FILE *f = fopen("prueba", "wb");
    for (valor = 0; valor < 20; valor++){
        convertido = htonl(valor);
        fwrite(&convertido, sizeof(uint32_t), 1, f);
    }
    fclose(f);
    f = fopen("prueba", "rb");
    while (!feof(f)){
        int leidos = fread(&valor, sizeof(uint32_t), 1, f);
        if (leidos > 0){
            convertido = ntohl(valor);
            printf("Lei %" PRIu32 " (%" PRIu32 ")\n", convertido, valor);
        }
    }
    fclose(f);
}
